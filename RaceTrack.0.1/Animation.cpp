#include "Animation.h"

Animation::Animation(sf::Texture * texture, sf::Vector2u imageCount, float switchTime)
{
	this->imageCount = imageCount;
	this->siwtchTime = switchTime;
	totalTime = 0.0f;
	currentImage.x = 0;

	uvRect.width = texture->getSize().x / float(imageCount.x);
	uvRect.height = texture->getSize().y / float(imageCount.y);
}

Animation::~Animation()
{
}

void Animation::update(int row , float deltaTime, bool faceRight)
{
	currentImage.y = row;
	totalTime += deltaTime ;

	if (totalTime >= siwtchTime)
	{
		totalTime -= siwtchTime;
		currentImage.x++;

		if (currentImage.x >= imageCount.x)
		{
			currentImage.x = 0;
		}
	}

	uvRect.top = currentImage.y * uvRect.height;

	if (faceRight)
	{
		uvRect.left = currentImage.x * uvRect.width;
		uvRect.width = abs(uvRect.width);
	}
	else
	{
		uvRect.left = (currentImage.x + 1) * abs(uvRect.width);
		uvRect.width = -abs(uvRect.width);
	}
}

bool Animation::interuptFrame(sf::Vector2u targerFrame)
{
	return (currentImage.x == targerFrame.x && currentImage.y == targerFrame.y);
}
