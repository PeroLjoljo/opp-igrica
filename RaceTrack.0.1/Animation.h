#pragma once
#include<SFML\Graphics.hpp>
#include<iostream>

class Animation {

	sf::Vector2u imageCount;
	
	float totalTime;
	float siwtchTime; 

public: 
	sf::Vector2u currentImage;
	Animation(sf::Texture* texture, sf::Vector2u imageCount, float switchTime);
	~Animation();

	void update(int row , float deltaTime, bool faceRight);
	bool interuptFrame(sf::Vector2u targerFrame);
	sf::IntRect uvRect;

};