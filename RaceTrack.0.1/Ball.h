#pragma once
#include"My_components.h"
#include"Car.h"
class Ball {

	sf::CircleShape redball;

	sf::Vector2f centerpoint; 
	sf::Vector2f direction; 
	sf::Vector2f up_down_vector;
	sf::Vector2f side_vector;
	sf::Vector2f bounce;
	
	bool jumprequest, shuldIchangedirection; 
	float* jumparr; 

	sf::Vector2f secondery_direction; 
	float radius; 
	float UpDown, side; 
	
public: 
	Ball();
	~Ball();
	sf::Vector2f getcenterpoint()const; 

	sf::Vector2f getdirection()const;

	void setrequest(bool);

	void updatecenterpoint();

	void changedirectiononbounce(bool);
	
	void updateUpDw();

	void gravitiypull();

	void incrementSide(float);

	void normaliseside();

	void setdirection(bool f);
	
	void slideingmotion(sf::Vector2f , sf::Vector2f);

	sf::Vector2f orientation(sf::Vector2f&, sf::Vector2f&);

	bool toporbottom(sf::Vector2f&, sf::Vector2f&);

	bool iscolided(sf::Vector2f);

	void setbounce(sf::Vector2f);

	void resetbounce();

	void setjump();

	void resetjump();

	sf::CircleShape& getrb();

};

void testbouncefunction(Ball&);

// opet si ga zakompliiro sa ovim bool funkcijama, skuzit kako pnormalno postavljat smijer loptice 




