#include"My_components.h"
#include"Car.h"
const float pi = 3.14159265358979323846f;

Car::Car()
{
	car.setSize(sf::Vector2f(20.0f, 12.0f));
	car.setOrigin(10.0f, 6.0f);
	car.setPosition(150.0f, 150.0f);
	car.rotate(160);
	Edge = new sf::Vector2f [4];
	acc = 0.0f;
	sideacc = 0.0f;
	ForwardVelocity = sf::Vector2f(0.0f, 0.0f);
	SideVelocity = sf::Vector2f(0.0f, 0.0f);
}
Car::~Car()
{
	delete[] Edge;
}
void Car::updateEdge()
{
	//rubovi "automobila"
	Edge[0] = (car.getTransform().transformPoint(car.getPoint(0)));
	Edge[1] = (car.getTransform().transformPoint(car.getPoint(1)));
	Edge[2] = (car.getTransform().transformPoint(car.getPoint(2)));
	Edge[3] = (car.getTransform().transformPoint(car.getPoint(3)));
}
//postavlja vektor ispred auta od midpoint
void Car::setForward()
{
	ForwardVelocity = sf::Vector2f(car.getPosition().x - 50.0f * cos((car.getRotation()*pi) / 180),
		+car.getPosition().y - 50.0f* sin((car.getRotation()*pi) / 180)) - car.getPosition();
}
//vektor "bocne" sile
void Car::setSide()
{
	SideVelocity = sf::Vector2f(car.getPosition().x - sideacc * sin((car.getRotation()*pi) / 180),
		+car.getPosition().y + sideacc*cos((car.getRotation()*pi) / 180)) - car.getPosition();
}
void Car::updateInput()
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
	{
		if (acc<600.0f)
			acc += 15.0f;
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		if (acc > -300.0f)
			acc -= 20.0f;
	}
	else if (acc > 0)
		acc -= 5.0f;
	else if (acc < 0)
		acc += 5.0f;
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		car.rotate(-4.0f);

		if (sideacc > -100.0f)
			sideacc -= 5.0f;
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		car.rotate(+4.0f);
		if (sideacc < 100.0f)
			sideacc += 5.0f;
	}
	else if (sideacc > 0.0f)
		sideacc -= 15.0f;
	else if (sideacc < 0)
		sideacc += 15.0f;

	car.move((getNorm(ForwardVelocity + SideVelocity) * (acc / 100)));

}
void Car::Draw(sf::RenderWindow & window)
{
	window.draw(car);
}
void Car::normaliseSideacc()
{
	sideacc = sideacc * 0.8f;
}

//fun uzima 4 tocke, kreira pravce AB CD, trazi presjeciste
sf::Vector2f intersection(sf::Vector2f a, sf::Vector2f b, sf::Vector2f c, sf::Vector2f d)
{
	float a1 = b.y - a.y;
	float b1 = a.x - b.x;
	float c1 = a1*(a.x) + b1*(a.y);
	
	float a2 = d.y - c.y;
	float b2 = c.x - d.x;
	float c2 = a2*(c.x) + b2*(c.y);
	//det 
	float det = a1*b2 - a2*b1;
	
	if (det == 0) {
		return sf::Vector2f(-1.0f, -1.0f);
	}
	else
	{
		//tocka sjecista
		sf::Vector2f pov;
		float x = (b2*c1 - b1*c2) / det;
		float y = (a1*c2 - a2*c1) / det;
		pov = sf::Vector2f(x, y);
		return pov;
	}
}
//fun prima dvije tocke linije i provjerava je li scjeciste u granicama
bool minmax(sf::Vector2f a, sf::Vector2f b, sf::Vector2f t)
{
	float minx, miny, maxx, maxy;
	if (a.x < b.x)
	{
		minx = a.x;
		maxx = b.x;
	}
	else
	{
		minx = b.x;
		maxx = a.x;
	}
	if (a.y < b.y)
	{
		miny = a.y;
		maxy = b.y;
	}
	else
	{
		miny = b.y;
		maxy = a.y;
	}
	if ((minx < t.x && t.x < maxx) && (miny < t.y && t.y < maxy))
		return true;
	else
		return false;
}

//fun uzima svaki rub automobila. provjerava sjeciste sa stazom
bool chekforintersections(Car& c, Track& t)
{
	sf::Vector2f point1, point2;
	sf::Vector2f intrsc;
	int edg1 = 0, edg2 = 1;

	for (unsigned i = 0; i < t.Outside.size() - 1; i++)
	{
		point1 = t.Outside.at(i);
		point2 = t.Outside.at(i + 1);
		edg1 = 0;
		edg2 = 1;
		for (int j = 0; j < 4; j++)
		{
			intrsc = intersection(c.getEdge(edg1), c.getEdge(edg2), point1, point2);

			if (minmax(c.getEdge(edg1), c.getEdge(edg2), intrsc) && minmax(point1, point2, intrsc))
				return true;

			edg1++;
			edg2++;
			if (edg1 == 3)
				edg2 = 0;
		}
	}
	if (!(t.Inside.empty()))
		for (unsigned k = 0; k < t.Inside.size() - 1; k++)
		{
			point1 = t.Inside.at(k);
			point2 = t.Inside.at(k + 1);
			edg1 = 0;
			edg2 = 1;
			for (int m = 0; m < 4; m++)
			{
				intrsc = intersection(c.getEdge(edg1), c.getEdge(edg2), point1, point2);

				if (minmax(c.getEdge(edg1), c.getEdge(edg2), intrsc) && minmax(point1, point2, intrsc))
					return true;

				edg1++;
				edg2++;
				if (edg1 == 3)
					edg2 = 0;
			}
		}
	return false;
}