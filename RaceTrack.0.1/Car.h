#pragma once
#include"My_components.h"
class Car {

	sf::RectangleShape car;
	sf::Vector2f SideVelocity;
	sf::Vector2f ForwardVelocity;
	sf::Vector2f *Edge;
	float acc;
	float sideacc;

public:
	Car();
	~Car();
	void updateEdge();
	void setForward();
	void normaliseSideacc();
	void setSide();
	void updateInput();
	void Draw(sf::RenderWindow&);
	sf::Vector2f getEdge(int i) { return Edge[i]; };

};

sf::Vector2f intersection(sf::Vector2f a, sf::Vector2f b, sf::Vector2f c, sf::Vector2f d);

bool minmax(sf::Vector2f a, sf::Vector2f b, sf::Vector2f t);

bool chekforintersections(Car& c, Track& t);