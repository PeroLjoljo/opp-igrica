#include "Characters.h"

Player::Player(sf::Texture * texture, sf::Vector2u imageCount, float switchTime, float speed)
	: animation(texture, imageCount, switchTime)
{
	this->speed = speed; 
	row = 0; 
	faceRight = true; 

	alive = true; 
	attackRange = 75.0f;
	getBody().setSize(sf::Vector2f(200.0f, 200.0f));
	getBody().setOrigin(100.0f, 100.0f);
	getBody().setTexture(texture);
	getBody().setPosition(800.0f, 400.0f);
	movement = sf::Vector2f(0.0f, 0.0f);

	startofanimation = false; 
}

Player::~Player()
{
}

void Player::Update(float deltaTime)
{
	bool flag = false; 
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
		movement.x -= speed * deltaTime;
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
		movement.x += speed * deltaTime;
	else
		movement.x = 0.0f;
	
	if (movement.x == 0.0f)
	{
		row = 1;
	}
	else {
		row = 0;

		if (movement.x > 0.0f)
			faceRight = true;
		else
			faceRight = false;

	}
	if (getBody().getPosition().y > 835.0f)
	{
		movement.y = 0; 
		flag = true; 
	}
	else {
		movement.y += 0.5f;
		row = 2;
		flag = false; 
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space) && flag == true && !startofanimation)
	{
		animation.currentImage.x = 0;
		startofanimation = true; 
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W) && flag == true)
	{
		movement.y -= 12.0f;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::R))
	{
		alive = true; 
		getBody().setPosition(sf::Vector2f(800.0f, 500.0f));
	}
	if (startofanimation)
	{
		row = 3;
		
	}
	if (animation.interuptFrame(sf::Vector2u(7, 3)))
		startofanimation = false;

	animation.update(row, deltaTime, faceRight);
	getBody().setTextureRect(animation.uvRect);
	getBody().move(movement);

}

bool Player::playerMadeHit()
{
	for (unsigned i = 3; i < 6; i++) // dio sprite sheeta gdje je "hit"
	{
		if (animation.interuptFrame(sf::Vector2u(i, 3)))
			return true;
	}
	return false;
}

Fireball::Fireball(sf::Texture * texture, sf::Vector2u imageCount, float switchTime, float speed, sf::Vector2f Position, bool moveright)
	: animation(texture, imageCount, switchTime)
{
	this->speed = speed;
	row = 0;
	faceRight = false; 

	body.setRadius(40.0f);
	body.setOrigin(15.0f, 15.0f);
	body.setTexture(texture);
	// **izvorna PNG textura wizarda je okrenuta u lijevo **
	float fireballoffset = 75.0f;
	if (!moveright)
		fireballoffset *= -1.0f;

	body.setPosition(Position.x + fireballoffset, Position.y);


	if(moveright)
		movement=sf::Vector2f(1.0f, 0.0f);
	else
		movement = sf::Vector2f(-1.0f, 0.0f);

}

Fireball::~Fireball()
{
	animation.~Animation();
}

void Fireball::Update(float deltaTime)
{
	row = 0; 
	animation.update(row, deltaTime, true);
	body.setTextureRect(animation.uvRect);
	body.move(movement* speed);
}

void Fireball::Draw(sf::RenderWindow & window)
{
    window.draw(body);
}

Wizard::Wizard(sf::Texture * texture, sf::Vector2u imageCount, float switchTime, float speed)
	: animation(texture, imageCount, switchTime)
{
	this->speed = speed;
	row = 0;
	faceRight = true;
	alive = true;
	getBody().setSize(sf::Vector2f(200.0f, 200.f));
	getBody().setOrigin(100.0f, 100.0f);
	getBody().setTexture(texture);
	getBody().setPosition(150.0f, 800.0f);
	movement = sf::Vector2f(0.0f, 0.0f);

	shoot = false;
	createProjectile = false;
}

Wizard::~Wizard()
{
	std::cout << "iM a firebaaaall tuu ritu tiru ru";
}

void Wizard::Update(float deltaTime, sf::Vector2f playerPosition)
{
	if (alive)
	{
		if (playerPosition.y > getBody().getPosition().y + 50.0f 
				|| abs(playerPosition.x - getBody().getPosition().x) > 200.0f)
		{
			row = 1;
		}
		if (playerPosition.y < getBody().getPosition().y + 50.0f && playerPosition.y > getBody().getPosition().y - 30.0f
			&& abs(playerPosition.x - getBody().getPosition().x) < 500.0f  &&  shoot == false)
		{
			shoot = true;
		}
		if (shoot && !createProjectile)
			row = 2;
		if (animation.interuptFrame(sf::Vector2u(8, 2)) && shoot == true)
		{
			createProjectile = true;

		}

		animation.update(row, deltaTime, false);
		getBody().setTextureRect(animation.uvRect);
		getBody().move(movement);
	}
}

Ghost::Ghost(sf::Texture * texture, sf::Vector2u imageCount, float switchTime, float speed)
	:animation(texture, imageCount, switchTime)
{

	this->speed = speed;
	row = 0;
	faceRight = true;
	alive = true;
	getBody().setSize(sf::Vector2f(200.0f, 200.f));
	getBody().setOrigin(100.0f, 100.0f);
	getBody().setTexture(texture);
	getBody().setPosition(400.0f, 800.0f);
	movement = sf::Vector2f(1.0f, 0.0f);

	hitframe = false;
	defenceframe = false;
	deathanimationstep = false;
	lefoversframe = false;
	ishit = false;
	redytostrike = false;
	strikedelay = 2.0f;
	activedefence = false;
	activesword = false; 
	defencedelay = 2.0f;
	
}

Ghost::~Ghost()
{
}

void Ghost::Update(float deltaTime)
{
	if (getBody().getPosition().x < 90.0f || getBody().getPosition().x > 1200.0f)
		faceRight = !faceRight;
	
	if (!faceRight)
	{
		movement.x = 1.2f;
	}
	else
	{
		movement.x = -1.2f;
	}
	//hod 
	row = 0;

	//udarac 
	if (redytostrike && strikedelay > 5.0f)
	{
		activesword = true; 
	}
	else if (redytostrike && defencedelay > 3.0f)
	{
		row = 2;
		activedefence = true; 
	}
	if (animation.interuptFrame(sf::Vector2u(0, 1)))
	{
		strikedelay = 0.0f;
	}
	if (animation.interuptFrame(sf::Vector2u(8, 1)))
	{
		redytostrike = false;
		activesword = false; 
		activedefence = true;
		animation.currentImage.x = 0; 
	}
	
	if (animation.interuptFrame(sf::Vector2u(7, 1)))
	{
		hitframe = true; 
	}
	else
	{
		hitframe = false; 
	}
	for (unsigned i = 1; i < 6; i++)
	{
		if (animation.interuptFrame(sf::Vector2u(i, 2)))
			defenceframe = true; 
	}
	if (animation.interuptFrame(sf::Vector2u(8, 2)))
	{
		activedefence = false;
		defencedelay = 0.0f;
		redytostrike = false; 
		animation.currentImage.x = 0; 
	}
	if (activesword)
	{
		row = 1; 
	}
	if (activedefence)
	{
		row = 2; 
	}

	//smrt
	if (ishit)
	{
		row = 4;
		movement.x = 0;
	}
	if (animation.interuptFrame(sf::Vector2u(8, 4)))
	{
		deathanimationstep = true;
	}
	if (deathanimationstep)
	{
		row = 5;
	}
	if (animation.interuptFrame(sf::Vector2u(8, 5)))
	{
		lefoversframe = true;
		deathanimationstep = false;
	}

	//ostatci 
	if (lefoversframe)
	{
		row = 6; 
		animation.currentImage.x = 0; 
		
	}
	strikedelay += 0.02f;
	defencedelay += 0.02f;

	animation.update(row, deltaTime, faceRight);
	getBody().setTextureRect(animation.uvRect);
	getBody().move(movement);

}

bool Ghost::direction(sf::Vector2f p)
{
	return getBody().getPosition().x < p.x;
}
