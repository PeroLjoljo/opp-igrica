#pragma once
#include"Animation.h"

class Character {

protected:
	sf::Vector2u imageCount;
	sf::RectangleShape body;
	sf::Vector2f movement;
	unsigned int row;
	float speed;
	bool faceRight;
	float attackRange; 
	bool alive;
	
public:
	virtual ~Character() {};
	void Draw(sf::RenderWindow& window) { if(alive) window.draw(body); };
	virtual sf::Vector2f getposition() const  { return body.getPosition(); }
	virtual void youAreDead() { alive = false; }
	sf::RectangleShape& getBody() { return body; }
	
	void setFaceRight(bool f) { faceRight = f;  }
	void setIsAlive(bool f) { alive = f; }
	bool ifisAlive() { return alive; }
};

class Player : public Character{

	Animation animation;
	bool startofanimation;
public:
	Player(sf::Texture* texture, sf::Vector2u imageCount, float switchTime, float speed);
	~Player();

	void Update(float deltaTime);
	bool playerMadeHit();

};

class Wizard : public Character {

	Animation animation;
	bool shoot;
	bool createProjectile;

public:

	Wizard(sf::Texture* texture, sf::Vector2u imageCount, float switchTime, float speed);
	~Wizard();

	void Update(float deltaTime, sf::Vector2f playerPosition);
 
	void redytoshoot() { shoot = false; }
	bool CanIcreateProjectile()const { return  createProjectile;  }
	void resetProjetile() { createProjectile = false;  shoot = false;}
	bool iscasterFaceRight()const { return faceRight;  }

};

class Fireball : public Character{

	Animation animation;
	sf::CircleShape body;

public:
	Fireball(sf::Texture* texture, sf::Vector2u imageCount, float switchTime, float speed, sf::Vector2f playerPosition, bool moveright);
	~Fireball();

	void Update(float deltaTimen);
	void Draw(sf::RenderWindow& window);
	sf::Vector2f getposition() const { return body.getPosition(); }

};

class Ghost : public Character{
	Animation animation; 
	bool hitframe;
	bool defenceframe;
	bool deathanimationstep;
	bool lefoversframe;
	bool ishit;
	bool redytostrike;
	float strikedelay;
	bool activedefence;
	bool activesword;
	float defencedelay;

public: 
	Ghost(sf::Texture* texture, sf::Vector2u imageCount, float switchTime, float speed);
	~Ghost(); 

	void setIfDead(bool f) { deathanimationstep = f; };
	void Update(float deltaTime);
	bool getHitFrame() { return hitframe; }
	void setRedyToStrike(bool f) { redytostrike = f; }
	bool direction(sf::Vector2f p);
	void setEndFrame(bool f) { ishit = f; }
	void resetEndFrameStep() { lefoversframe = false;  }
	bool isDefenceAtive()const { return activedefence; }
	
	//bool directionLR;
};