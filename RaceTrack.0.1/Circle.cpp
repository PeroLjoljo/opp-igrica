#include"Circle.h"

void setallCircles(std::vector<my_circle>& v)
{
	my_circle x;
	x.newCircle(800.0f, 450.0f, 50.0f);
	x.c.setFillColor(sf::Color::Red);
	v.push_back(x);
	x.newCircle(400.0f, 500.0f, 30.0f);
	x.c.setFillColor(sf::Color::Blue);
	v.push_back(x);
	x.newCircle(400.0f, 200.0f, 35.0f);
	x.c.setFillColor(sf::Color::Blue);
	v.push_back(x);
	x.newCircle(200.0f, 250.0f, 25.0f);
	x.c.setFillColor(sf::Color::Blue);
	v.push_back(x);
	x.newCircle(100.0f, 500.0f, 40.0f);
	x.c.setFillColor(sf::Color::Blue);
	v.push_back(x);
	x.newCircle(1100.0f, 300.0f, 60.0f);
	x.c.setFillColor(sf::Color::Blue);
	v.push_back(x);
	x.newCircle(700.0f, 600.0f, 30.0f);
	x.c.setFillColor(sf::Color::Blue);
	v.push_back(x);
	x.newCircle(1000.0f, 500.0f, 60.0f);
	x.c.setFillColor(sf::Color::Blue);
	v.push_back(x);
	x.newCircle(150.0f, 700.0f, 70.0f);
	x.c.setFillColor(sf::Color::Blue);
	v.push_back(x);
	x.newCircle(850.0f, 200.0f, 70.0f);
	x.c.setFillColor(sf::Color::Blue);
	v.push_back(x);
	x.newCircle(1050.0f, 750.0f, 70.0f);
	x.c.setFillColor(sf::Color::Blue);
	v.push_back(x);
	x.newCircle(350.0f, 800.0f, 50.0f);
	x.c.setFillColor(sf::Color::Blue);
	v.push_back(x);
	x.newCircle(650.0f, 100.0f, 60.0f);
	x.c.setFillColor(sf::Color::Blue);
	v.push_back(x);
	x.newCircle(150.0f, 150.0f, 80.0f);
	x.c.setFillColor(sf::Color::Blue);
	v.push_back(x);

}

sf::Vector2f getNormC(sf::Vector2f a)
{
	return  a / sqrt(pow(a.x, 2) + pow(a.y, 2));
}

bool circleDistance(my_circle a, my_circle b)
{
	float distance = sqrt(pow((b.c.getPosition().x - (a.c.getPosition().x)), 2)
		+ pow((b.c.getPosition().y - a.c.getPosition().y), 2));

	if (a.c.getRadius() + b.c.getRadius() > distance)
		return true;
	else
		return false;

}
void updateDirection(my_circle& c1, my_circle& c2)
{
	c1.crcAcc = c2.crcAcc = (c1.crcAcc + c2.crcAcc) / 2;

	c1.direction = getNormC(c1.direction + getNormC(c1.c.getPosition() - c2.c.getPosition()));
	c2.direction = getNormC(c2.direction + getNormC(c2.c.getPosition() - c1.c.getPosition()));
}
void findCollision(std::vector<my_circle>& v)
{

	for (unsigned i = 0; i < v.size(); i++)
	{
		if (v.at(i).crcAcc>0.0f)
			for (unsigned j = 0; j < v.size(); j++)
			{
				if (i != j)
					if (circleDistance(v.at(i), v.at(j)))
					{
						if (i == 0)
							v.at(i).crcAcc /= 1.5f;
						updateDirection(v.at(i), v.at(j));
					}
			}
	}

}
void windowBounds(std::vector<my_circle>& v)
{
	for (unsigned i = 0; i < v.size(); i++)
	{
		if (v.at(i).c.getPosition().x - v.at(i).c.getRadius() < 0 || (v.at(i).c.getPosition().x + v.at(i).c.getRadius()) > 1300.0f)
		{
			v.at(i).direction.x = v.at(i).direction.x * -1.0f;

		}
		if (v.at(i).c.getPosition().y - v.at(i).c.getRadius() < 0.0f || (v.at(i).c.getPosition().y + v.at(i).c.getRadius()) > 900.0f)
		{
			v.at(i).direction.y *= -1.0f;
		}
	}
}
my_circle my_circle::newCircle(float a, float b, float r)
{

	c.setRadius(r);
	c.setOrigin(sf::Vector2f(r, r));
	c.setPosition(a, b);
	c.setFillColor(sf::Color::White);
	crcAcc = 0.0f;
	direction = sf::Vector2f(0.0f, 0.0f);
	return *this;

}

void my_circle::UpdateInput()
{
	bool keypress = false;
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
	{
		if (direction.y > -1.0f)
			direction.y -= 0.02f;
		if (crcAcc < 2.0f)
			crcAcc += 0.01f;
		keypress = true;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		if (direction.y < 1.0f)
			direction.y += 0.02f;
		if (crcAcc < 2.0f)
			crcAcc += 0.01f;
		keypress = true;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		if (direction.x > -1.0f)
			direction.x -= 0.02f;
		if (crcAcc < 2.0f)
			crcAcc += 0.01f;
		keypress = true;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		if (direction.x < 1.0f)
			direction.x += 0.02f;
		if (crcAcc < 2.0f)
			crcAcc += 0.01f;
		keypress = true;
	}
	if (crcAcc > 0.0f && !keypress)
		crcAcc -= 0.01f;
}

void my_circle::UpdatePositoins(bool f)
{
	if(!f)
		c.move(direction * crcAcc);
	else if (f)
	{
		c.move(direction *(crcAcc*10.0f));

		if (crcAcc > 0.0f)
			crcAcc -= 0.0005f;
		if (crcAcc < 0.0f)
			crcAcc = 0.0f;

		if (crcAcc == 0.0f)
		{
			direction = sf::Vector2f(0.0f, 0.0f);
		}
	}

}
