	#pragma once
#include <SFML/Graphics.hpp>
#include<iostream>
#include<math.h>
#include<vector>

class my_circle {

public:
	sf::CircleShape c;
	sf::Vector2f direction;
	float inputacc;
	float crcAcc;

	void UpdateInput();
	void UpdatePositoins(bool f);
	
	my_circle newCircle(float a, float b, float r);

};

void setallCircles(std::vector<my_circle>& v);

sf::Vector2f getNormC(sf::Vector2f a);

void windowBounds(std::vector<my_circle>& v);

void findCollision(std::vector<my_circle>& v);
