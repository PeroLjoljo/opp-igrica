#include"Logic.h"

static int inorout = 0;

void CircleLogic(std::vector<my_circle>& circles)
{
	circles.at(0).UpdateInput();
	circles.at(0).UpdatePositoins(false);
	findCollision(circles);
	windowBounds(circles);
	for (unsigned i = 1; i < circles.size(); i++)
	{
		circles.at(i).UpdatePositoins(true);
	}
}

void CarLogic(Car& c, Track& track, sf::RenderWindow& window, sf::CircleShape redgreen)
{

	c.setForward();
	c.updateEdge();
	c.setSide();
	c.normaliseSideacc();

	c.updateInput();
	
	if (!(track.Outside.empty()))
		if (chekforintersections(c, track))
		{
			std::cout << "\n Overlap";
			redgreen.setFillColor(sf::Color::Red);
			window.draw(redgreen);
		}
		else
		{
			redgreen.setFillColor(sf::Color::Green);
			window.draw(redgreen);
			std::cout << "\n";
		}
	c.Draw(window);


	//etTrack(track, inorout ,window);
	/*if (sf::Keyboard::isKeyPressed(sf::Keyboard::U))
	{
	if (inorout == 0)
	inorout = 1;
	else if (inorout==1)
	{
	exportTrack(track);
	}
	}  //  <---- crtanje staze -_-*/

}

void gameLogic(Player& player, Wizard& wizard1, Ghost& ghost, std::vector<Fireball>& projettilevector, sf::RenderWindow& window, float deltaTime, sf::Texture& t)
{
	player.Update(deltaTime);
	wizard1.Update(deltaTime, player.getposition());
	ghost.Update(deltaTime);

	if (wizard1.CanIcreateProjectile())
	{
		float fireballoffset = 25.0f;
		if (wizard1.iscasterFaceRight())
			fireballoffset *= -1.0f;

		Fireball* fireball = new Fireball(&t, sf::Vector2u(6, 1), 0.01f, 11.50f,
			sf::Vector2f(wizard1.getposition().x + fireballoffset, wizard1.getposition().y), wizard1.iscasterFaceRight());
		projettilevector.push_back(*fireball);
		delete fireball; 
	}

	window.clear();

	if (!projettilevector.empty())
	{
		if (abs(player.getposition().x - projettilevector.at(0).getposition().x) < 40.0f && abs(player.getposition().y - projettilevector.at(0).getposition().y) < 50.0f  && player.ifisAlive())
			player.youAreDead();
		projettilevector.at(0).Update(deltaTime);
		projettilevector.at(0).Draw(window);
	}

	if (!projettilevector.empty() && projettilevector.at(0).getposition().x > 1200.0f &&  player.ifisAlive())
	{
		wizard1.resetProjetile();
		projettilevector.clear();
	}
	if (player.playerMadeHit() && abs(player.getposition().x - wizard1.getposition().x) < 150.0f)
		wizard1.youAreDead();

	if (abs(player.getposition().x - ghost.getposition().x) < 140.0f && abs(player.getposition().y - ghost.getposition().y) < 120.0f)
	{
		if(player.playerMadeHit() && !ghost.isDefenceAtive())
			ghost.setEndFrame(true);
		if (ghost.getHitFrame())
			player.youAreDead();
	}
	if (abs(player.getposition().x - ghost.getposition().x) < 200.0f && player.ifisAlive())
	{
		ghost.setRedyToStrike(true);
	}
	if (ghost.direction(player.getposition()) && player.ifisAlive())
	{
		ghost.setFaceRight(false);
	}
	else if (player.ifisAlive())
	{
		ghost.setFaceRight(true);
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::T))
	{
		ghost.setEndFrame(false);
		ghost.resetEndFrameStep(); 
		wizard1.setIsAlive(true);
	}

	wizard1.Draw(window);
	player.Draw(window);
	ghost.Draw(window);
	window.display();
}
