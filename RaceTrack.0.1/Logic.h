#pragma once

#include <SFML/Graphics.hpp>
#include<iostream>
#include "My_components.h"
#include"Circle.h"
#include"Car.h"
#include"Animation.h"
#include"Characters.h"



void CircleLogic(std::vector<my_circle>& circles);
void CarLogic(Car& c, Track& track, sf::RenderWindow& window, sf::CircleShape redgreen);
void gameLogic(Player&, Wizard&, Ghost&, std::vector<Fireball>&, sf::RenderWindow& window, float deltaTime, sf::Texture&);
