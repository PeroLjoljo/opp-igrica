#include"My_components.h"

My_VertexDrawSet::My_VertexDrawSet()
{
	sf::VertexArray a;
	trackVertexOut.setPrimitiveType(sf::LineStrip);
	trackVertexOut.resize(1000);
	trackVertexIn.setPrimitiveType(sf::LineStrip);
	trackVertexIn.resize(1000);

}

void My_VertexDrawSet::Draw(sf::RenderWindow& window)
{
	if (trackVertexIn.getVertexCount() != 0)
		window.draw(trackVertexIn);
	if (trackVertexOut.getVertexCount() != 0)
		window.draw(trackVertexOut);
}

// vrati jedinicni vektor (strijelicu smijera)
sf::Vector2f getNorm(sf::Vector2f a)
{
	return  a / sqrt(pow(a.x, 2) + pow(a.y, 2));
}

void chekDoublePoint(Track& track)
{
	std::vector<sf::Vector2f> tempVec1;
	std::vector<sf::Vector2f> tempVec2;
	bool flag = true;

	for (unsigned i = 0; i < track.Inside.size(); i++)
	{
		if (tempVec1.empty())
			tempVec1.push_back(track.Inside.at(0));

		for (unsigned j = 0; j < tempVec1.size(); j++)
			if (track.Inside.at(i).x == tempVec1.at(j).x && track.Inside.at(i).y == tempVec1.at(j).y)
				flag = false;

		if (flag)
			tempVec1.push_back(track.Inside.at(i));
		else
			flag = true;
	}
	if (!(track.Inside.empty()))
		tempVec1.push_back(track.Inside.at(0));

	flag = true; 
	for (unsigned i = 0; i < track.Outside.size(); i++)
	{
		if (tempVec2.empty())
			tempVec2.push_back(track.Outside.at(0));

		for (unsigned j = 0; j < tempVec2.size(); j++)
			if (track.Outside.at(i).x == tempVec2.at(j).x && track.Outside.at(i).y == tempVec2.at(j).y)
				flag = false;

		if (flag)
			tempVec2.push_back(track.Outside.at(i));
		else
			flag = true;
	}
	if (!(track.Outside.empty()))
		tempVec2.push_back(track.Outside.at(0));

	track.Inside = tempVec1;
	track.Outside = tempVec2;
	tempVec1.clear();
	tempVec2.clear();
}
void setTrack(Track& vecTrack, int inORout, sf::RenderWindow& window)
{	
	//crtanje staze -_-
	if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
	{
		sf::Vector2f a = sf::Vector2f(sf::Mouse::getPosition(window));
		if (inORout == 0)
			vecTrack.Outside.push_back(a);
		else if (inORout == 1)
			vecTrack.Inside.push_back(a);
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::L))
	{
		if (inORout == 0)
			vecTrack.Outside.push_back(vecTrack.Outside.front());
		else if (inORout == 1)
			vecTrack.Inside.push_back(vecTrack.Inside.front());
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::N))
	{
		if (inORout == 0)
			vecTrack.Outside.pop_back();
		if (inORout == 1)
			vecTrack.Inside.pop_back();
	}

	chekDoublePoint(vecTrack);
	
}

void setVertex(Track& trk, My_VertexDrawSet& vtx)
{
	unsigned i = trk.Inside.size();
	unsigned j = trk.Outside.size();

	vtx.trackVertexIn.clear();
	vtx.trackVertexOut.clear();
		
	if (!trk.Inside.empty())
	{
		for (unsigned n = 0; n < trk.Inside.size(); n++)
		{
			vtx.trackVertexIn.append(trk.Inside.at(n));
		}
	}
	if (!trk.Outside.empty())
	{
		for (unsigned m = 0; m < trk.Outside.size(); m++)
		{
			vtx.trackVertexOut.append(trk.Outside.at(m));
		}

	}
}

void exportTrack(Track track)
{
	std::fstream f;
	f.open("track.txt");

	//zbog freamelimita(60) mouse::getpoint() stvara vise istih tocaka,
	//filtriraj ponavljanja
	chekDoublePoint(track);

	if (f.is_open())
	{
		f << "Out\n";
		for (unsigned i = 0; i < track.Outside.size(); i++)
		{
			f << track.Outside.at(i).x;
			f << " ";
			f << track.Outside.at(i).y;
			f << "\n";
		}
		f << "In\n";
		for (unsigned i = 0; i < track.Inside.size(); i++)
		{
			f << track.Inside.at(i).x;
			f << " ";
			f << track.Inside.at(i).y;
			f << "\n";
		}
	}
	f << "End of Track";
	f.close();
}

void inportTrack(Track& track)
{
	std::fstream f;
	f.open("track.txt");
	std::string buffer; 
	int a;
	float x= 0.0f, y = 0.0f;
	bool xyflag=true;
	bool inOutflag = true;
	if (f.is_open())
	{

		std::getline(f, buffer);
		
		while (true)
		{
			std::getline(f, buffer);
			x = y = 0.0f;
			if (buffer == "In")
			{
				inOutflag = false; 
			}
			else if (buffer == "End of Track")
				break;

			xyflag = true;
			for (unsigned i = 0; i < buffer.size(); i++)
			{
				if (buffer.at(i) == ' ')
				{
					xyflag = false;
				}
				if (isdigit(buffer.at(i)))
				{
					a = buffer.at(i) - 48;
					if (xyflag)
					{
						x *= 10;
						x += a;
					}
					else
					{
						y *= 10;
						y += a;
					}
				}
			}
			if(inOutflag)
				track.Outside.push_back(sf::Vector2f(x, y));
			else 
				track.Inside.push_back(sf::Vector2f(x, y));
			
		}	
	}
}



 
