#pragma once
#include <SFML/Graphics.hpp>
#include<iostream>
#include<cmath>
#include<vector>
#include<fstream>
#include<SFML/Graphics/Rect.hpp>

class Track {

public:
	std::vector<sf::Vector2f> Inside;
	std::vector<sf::Vector2f> Outside;

};

class My_VertexDrawSet {

public:
	sf::VertexArray trackVertexOut;
	sf::VertexArray trackVertexIn;

	My_VertexDrawSet();

	void Draw(sf::RenderWindow& window);
};

sf::Vector2f getNorm(sf::Vector2f a);

void setTrack(Track& vecTrack, int inORout, sf::RenderWindow& window);

void setVertex(Track& trk, My_VertexDrawSet& vtx);

void exportTrack(Track track);

void inportTrack(Track& track);

