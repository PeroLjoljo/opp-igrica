#include "My_components.h"
#include"Circle.h"
#include"Car.h"
#include"Ball.h"
#include"Animation.h"
#include"Logic.h"
#include"Characters.h"

int main()
{
	sf::ContextSettings settings;
	settings.antialiasingLevel = 8;

	sf::RenderWindow window(sf::VideoMode(1300,900), "Race Track", sf::Style::Close,settings);
	window.setFramerateLimit(60);

	Car c;
	Track track;
	My_VertexDrawSet drawTrack;
	inportTrack(track);
	//setVertex(track, drawTrack);
	sf::CircleShape redgreen(100.0f);
	redgreen.setPosition(sf::Vector2f(400.0f, 350.0f));

	std::vector<my_circle>	circles;

	setallCircles(circles);
	float acc=0.0f;
	int mod=1;
	
	sf::Texture playertexture;
	if (!playertexture.loadFromFile("player.png"))
		throw "cannot load player";
	Player player(&playertexture, sf::Vector2u(8, 4), 0.1f, 5.0f);
	
	sf::Texture wizadtexture;
	if (!wizadtexture.loadFromFile("wizardNPC.png"))
		throw "cannot load wizard";
	Wizard wizard1(&wizadtexture,sf::Vector2u(13,3),0.1f,10.0f);
	
	std::vector<Fireball> projettilevector;
	sf::Texture fireballtexture;
	if (!fireballtexture.loadFromFile("fireball2.png"))
		throw "cannot load fireball";
	
	sf::Texture ghosttexture; 
	if (!ghosttexture.loadFromFile("ghost.png"))
		throw "cannot load ghost";
	Ghost ghost(&ghosttexture, sf::Vector2u(9, 7), 0.1f, 8.0f);
	 
	float deltaTime=0.0f; 
	sf::Clock clock; 

	bool flg = false; 
	
	while (window.isOpen())
	{

		sf::Event event;
		while (window.pollEvent(event))
			if (event.type == event.Closed)
				window.close();

		deltaTime = clock.restart().asSeconds();

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num2))
			mod = 2;
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num1))
			mod = 1;
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num3))
			mod = 3;

		if (mod == 2)
		{
			window.clear();

			CarLogic(c, track,window, redgreen);
			setVertex(track, drawTrack); //kod crtanja mora biti tu , ako se loada staza iz datoteke, zakomentiraj

			drawTrack.Draw(window);

			window.display();
		}
		if (mod == 1)
		{
			CircleLogic(circles);

			window.clear();
			window.draw(circles.at(0).c);
			for (unsigned i = 1; i < circles.size(); i++)
			{
				window.draw(circles.at(i).c);
			}
			window.display();
		}
		
		if (mod == 3)
		{
			gameLogic(player, wizard1, ghost, projettilevector, window, deltaTime, fireballtexture);
		}
	}
	projettilevector.clear();
	return 0;
}

